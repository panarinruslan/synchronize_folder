Local installation:

pip install path_to_dir
if this is copied to the given directory then:
pip install .

Collected dist but I'm not published on PyPl

Synchronizes the given list of directories (for synchronized use, you can set the parameter num_cpu=1)

Required parameters:
1. Primary synchronization directory - to bring all folders to a single state, all directories at the first poll.

2. List of directories for synchronization. The directory from the first point will be included in the shared pool after the first synchronization.

Usage:

    from monitor_sync_dir import main_monitoring
    main_monitoring([...#sync dirs]: list, "#path to main sync dir": str, #number of cpu usage: int or None, #number iterations: int or None)

Windows usage:

    from monitor_sync_dir import main_monitoring
    if __name__ == "__main__":
        main_monitoring([...#sync dirs]: list, "#path to main sync dir": str, #number of cpu usage: int or None, #number iterations: int or None)



Next updates:

Adding an asynchronous function to check the status of folders. This will give rise to the problem of concurrent file modification or addition conflicts (based on file last modified time, no such problem for directories).

Adding a function when moving files inside a synchronized directory, since the move function is an order of magnitude faster than the copy function

Can be done:
Binding to specific platforms such as Linux to track directory change signals

Changes when using third-party libraries:
1. You can use libraries like watchdog to generate a signal on any change to files in directories.
2. You can think of a more complete system with Celery and Redis/Rabbit MQ to track signals.


Note:
If the files themselves are small size and the changes are not large, then the speed increase for multiprocessor use will not be very noticeable (starting a new process may take longer than traversing folders). This will happen with the presented test files. As the size of the files and the number of folders increase, there will be a noticeable increase in speed from the multiprocessor approach.