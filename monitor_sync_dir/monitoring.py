"""
Synchronizing directories with only python standard libraries using multiprocessors
Author: Panarin Ruslan
email: panarinruslan@gmail.com
License: MIT License
Year: 2022
"""

import os
import pathlib
import concurrent.futures as pool
from multiprocessing import cpu_count
import shutil
import time
import statistics


def check_change(sync_dir: str, main_set: list, first_check=False):
    """
    Compares directory images (checked and main). At the moment,
    it checks only changes, without taking into account the date of the changes.
    :param sync_dir: checked directory
    :param main_set: master image (last synced)
    :param first_check: Parameter indicating the first synchronization.
    It is necessary that the first time the folders are synchronized only with the main directory
    :return: Returns a dictionary of changes - deletion and addition
    """

    image_set = create_image_set(sync_dir)

    if image_set != main_set:
        changes_check_to_main = [el for el in main_set if el not in image_set]
        changes_main_to_check = [el for el in image_set if el not in main_set]

        if first_check:
            return {
                "remove": changes_main_to_check,
                "copy": changes_check_to_main
            }, main_set

        else:
            return {
                "remove": changes_check_to_main,
                "copy": changes_main_to_check
            }, image_set

    else:
        return {
            "remove": [],
            "copy": []
        }, image_set


def create_obj_image(path: str, last_changed: float or None, type_object: str) -> dict:
    """
    Creates an image object of the searched directory
    :param path: Image object path
    :param last_changed: Last modified time. It is necessary to track whether the file has changed
    :param type_object: Type of object in directory - folder or file
    :return: Returns a directory image dictionary object
    """

    return {
        'path': path,
        'last_change': last_changed,
        'type_object': type_object
    }


def create_image_set(dir_for_set: str) -> list:
    """
    Creates a directory image
    :param dir_for_set: Directory path
    :return: Image directory
    """
    main_set = list()

    for root, dirs, files in os.walk(dir_for_set):

        if root[len(dir_for_set):] != 0:
            main_set.append(create_obj_image(root[len(dir_for_set)+1:], None, 'folder'))

        for file in files:
            if root[len(dir_for_set):] == 0:
                main_set.append(
                    create_obj_image(
                        file,
                        pathlib.Path(os.path.join(root, file)).stat().st_mtime,
                        'file'
                    )
                )
            else:
                main_set.append(
                    create_obj_image(
                        os.path.join(root[len(dir_for_set)+1:], file),
                        pathlib.Path(os.path.join(root, file)).stat().st_mtime,
                        'file'
                    )
                )

    return main_set


def calc_dirs_for_workers(workers: int, sync_dirs: list, main_dir: str) -> list:
    """
    Calculates the number of directories per processor
    :param workers: Number of available processors
    :param sync_dirs: Number of processors available (either user defined or available in general)
    :param main_dir: The directory on the basis of which the image is built,
    the parameter is used to exclude from the general list of directories
    :return: Returns a list of directories for each processor
    """

    change_dirs_or = [dir_sync for dir_sync in sync_dirs if dir_sync != main_dir]

    if len(change_dirs_or) >= workers:

        dirs_per_core = len(change_dirs_or) // workers

        change_dirs = list()

        for i in range(0, workers, 1):
            if i != workers - 1:
                change_dirs.append(
                    change_dirs_or[i * dirs_per_core: (i + 1) * dirs_per_core]
                )
            else:
                change_dirs.append(
                    change_dirs_or[i * dirs_per_core:]
                )
    else:
        change_dirs = change_dirs_or

    return change_dirs


def synchronize_dirs(sync_dirs: list, main_set: list, first_check: bool, num_cpu: int or None):
    """
    Basic Directory Synchronization Function with multiprocessing (lib - concurrent.futures)
    :param sync_dirs: List of synchronized directories
    :param main_set: Last synced image
    :param first_check: Parameter indicating the first synchronization.
    It is necessary that the first time the folders are synchronized only with the main directory
    :param num_cpu: The number of available processors.
    If the parameter is not specified, it will be calculated based on multiprocessing.cpu_count()
    :return: Returns the execution time of the sync function and the new synced directory image
    """

    start_time = time.time()

    if num_cpu is None:
        num_cpu = cpu_count()  # number of cpu cores for multiprocessing tools -> each equal loading

    if num_cpu >= len(sync_dirs)-1:
        workers = len(sync_dirs)-1
    else:
        workers = num_cpu

    if first_check:
        with pool.ProcessPoolExecutor(workers) as executor:

            change_dirs = calc_dirs_for_workers(workers, sync_dirs, sync_dirs[0])

            futures = [
                executor.submit(
                    sync_directory,
                    main_set=main_set, from_dir=sync_dirs[0],
                    to_dirs=to_dirs, first_check=first_check, changes_set=None
                ) for to_dirs in change_dirs
            ]

            pool.wait(futures)

    else:

        for directory in sync_dirs:

            change_dirs = calc_dirs_for_workers(workers, sync_dirs, directory)
            changes_set, main_set = check_change(directory, main_set, first_check)

            with pool.ProcessPoolExecutor(workers) as executor:

                futures = [
                    executor.submit(
                        sync_directory,
                        main_set=main_set, from_dir=directory,
                        to_dirs=to_dirs, first_check=first_check, changes_set=changes_set
                    ) for to_dirs in change_dirs
                ]

                pool.wait(futures)

    end_time = time.time()

    return end_time - start_time, main_set


def sync_directory(main_set: list, from_dir: str, to_dirs: list, first_check: bool, changes_set: list or None) -> None:
    """
    Directory synchronization function for each processor
    :param main_set: Last synced image
    :param from_dir: The path to the directory will be migrated objects
    :param to_dirs: Path to the directory to which the objects are transferred
    :param first_check: Parameter indicating the first synchronization.
    It is necessary that the first time the folders are synchronized only with the main directory
    :param changes_set: Dictionary of changes
    """

    for to_dir in to_dirs:
        if first_check:
            changes_set, new_main_set = check_change(to_dir, main_set, first_check)
        if len(changes_set['copy']) != 0 or len(changes_set['remove']) != 0:
            print(f'Changes for {to_dir}: {changes_set}')

            for remove_obj in changes_set['remove']:
                path_remove = pathlib.Path(to_dir).joinpath(remove_obj['path'])
                if pathlib.Path(path_remove).exists():
                    if pathlib.Path(path_remove).is_dir():
                        shutil.rmtree(path_remove)
                    else:
                        os.remove(path_remove)

            for add_obj in changes_set['copy']:
                path_copy_in = os.path.join(from_dir, add_obj['path'])
                path_copy_to = os.path.join(to_dir, add_obj['path'])
                if not pathlib.Path(path_copy_to).exists():
                    if add_obj['type_object'] == 'folder':
                        shutil.copytree(path_copy_in, path_copy_to)
                    else:
                        shutil.copy2(path_copy_in, path_copy_to)


def main_monitoring(folder_directory: list, main_src_dir: str, num_cpu: list or None, iterations: int or None):
    """
    Main sync trigger function
    :param folder_directory: List of synchronized directories
    :param main_src_dir: First Synchronization Directory
    :param num_cpu: number of processors specified by the user
    :param iterations: number iterations for a time test
    :return: None
    """

    if main_src_dir not in folder_directory:
        folder_directory.insert(0, main_src_dir)

    main_src_set = create_image_set(main_src_dir)

    work_to_sync, main_src_set = synchronize_dirs(folder_directory, main_src_set, True, num_cpu)
    print('Sync in: ', work_to_sync, 'sec')

    iteration = 1
    re_run = True
    time_sync_list = list()


    while re_run:
        work_to_sync, main_src_set = synchronize_dirs(folder_directory, main_src_set, False, num_cpu)
        print('Sync in: ', work_to_sync, 'sec')
        time_sync_list.append(work_to_sync)
        iteration += 1
        if iteration > iterations:
            re_run = False

    print(statistics.mean(time_sync_list))