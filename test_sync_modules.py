from monitor_sync_dir.monitoring import main_monitoring

# need for windows multiprocessing tools
if __name__ == "__main__":
    # your can use archive Test_folder_dir.rar in git for your testing or use your directory
    test_dirs = [
        r'C:\Test_folder_dir\folder_4',
        r'C:\Test_folder_dir\folder_3',
        r'C:\Test_folder_dir\folder_1',
        r'C:\Test_folder_dir\folder_2',
        r'C:\Test_folder_dir\folder_5'
    ]

    main_src_dir = r'C:\Test_folder_dir\folder_6'

    main_monitoring(test_dirs, main_src_dir, None, 10)

